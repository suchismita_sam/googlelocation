//
//  ViewController.m
//  googleLocation
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

CLLocationManager *manager;
GMSMarker *mark;
CLLocation *currentLocation;
CLLocationCoordinate2D position;
NSMutableArray *city;
NSMutableArray *destdata;
NSMutableArray *address;
CLPlacemark *placemark;
NSString *name;
NSDictionary *addressDictionary;
NSString *state;
NSString *cityName;
NSString *country;
CLLocationManager *locationManager;

@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *googleMapImage;
@property (strong, nonatomic) IBOutlet GMSMapView *googleMap;
@property (strong, nonatomic) IBOutlet UIButton *getDetailsButton;
@property (strong, nonatomic) IBOutlet UILabel *l;

@end


@implementation ViewController

@synthesize getDetailsButton;
@synthesize googleMap;
@synthesize googleMapImage;
@synthesize l;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    city=[NSMutableArray new];
    locationManager = [[CLLocationManager alloc] init];
    manager = [[CLLocationManager alloc]init];
    googleMap.delegate=self;
    manager.delegate= self;
    manager.desiredAccuracy = kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
//    [self location];
    
    

    

    // Do any additional setup after loading the view, typically from a nib.
}

////CURRENT LOCATION
//- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
//{
//    UIAlertView *errorAlert = [[UIAlertView alloc]
//                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [errorAlert show];
//}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    currentLocation = newLocation;
//    if (currentLocation != 0)
//    {
//        position = currentLocation.coordinate;
//        mark = [GMSMarker markerWithPosition:currentLocation.coordinate];
//        mark.title = @"current loation";
//        mark.map = googleMap;
//        mark.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
//        [city addObject:[NSString stringWithFormat:@"%.4f",currentLocation.coordinate.longitude]];
//        [city addObject:[NSString stringWithFormat:@"%.4f",currentLocation.coordinate.latitude]];
//        CLGeocoder *reverseGeocoder = [[CLGeocoder alloc] init];
//        
//        [reverseGeocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
//        {
//
//            CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
//            NSString *countryCode = myPlacemark.ISOcountryCode;
//            NSString *countryName = myPlacemark.country;
//            NSString *cityName= myPlacemark.subAdministrativeArea;
//            [city addObject:[NSString stringWithFormat:@"%@",cityName]];
//            
//            NSString *stateName= myPlacemark.administrativeArea;
//            [city addObject:[NSString stringWithFormat:@"%@",stateName]];
//            
//            NSString *area= myPlacemark.country;
//            [city addObject:[NSString stringWithFormat:@"%@",area]];
//            
//            NSLog(@"My country code:" @"%@" @"and countryName:" @"%@" @"MyCity:" @"%@", countryCode, countryName, cityName);
//            NSDictionary *addressDictionary =
//            myPlacemark.addressDictionary;
//            NSArray *show= addressDictionary[@"FormattedAddressLines"];
//            country=[show componentsJoinedByString:@","];
//            [city addObject:country];
//            NSLog(@"%@",country);
//            
//        }];
//    }
//}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"next"])
    {
        DestinationViewController *dest=segue.destinationViewController;
        dest.destdata=city;
    }
}

////current location
//
//-(void)location
//
//{
//    
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//    
//    
//    
//    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:position.latitude
//                               
//                                                        longitude:position.longitude];
//    
//    
//    
//    [geocoder reverseGeocodeLocation:newLocation
//     
//                   completionHandler:^(NSArray *placemarks, NSError *error) {
//                       
//                       
//                       
//                       if (error) {
//                           
//                           NSLog(@"Geocode failed with error: %@", error);
//                           
//                           return;
//                           
//                       }
//                       
//                       
//                       
//                       if (placemarks && placemarks.count > 0)
//                           
//                       {
//                           
//                           CLPlacemark *placemark = placemarks[0];
//                           
//                           
//                           
//                           NSDictionary *addressDictionary =
//                           
//                           placemark.addressDictionary;
//                           
//                           
//                           
//                           NSLog(@"%@ ", addressDictionary) ;
//                           
//                           
//                           
//                       }
//                       
//                   }];}

//ON CLICK MARKER IS BEEN CREATED
-(NSString *) getlocation: (CLLocationCoordinate2D )newLocation
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    CLLocation *nlocation = [[CLLocation alloc]initWithLatitude:newLocation.latitude
                                                      longitude:newLocation.longitude];
    
    [geocoder reverseGeocodeLocation :nlocation
                    completionHandler:^(NSArray *placemarks, NSError *error)
     {
         
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             return;
         }
         if (placemarks && placemarks.count > 0)
         {
             CLPlacemark *placemark = placemarks[0];
             NSDictionary *addressDictionary =
             placemark.addressDictionary;
             NSLog(@"%@",addressDictionary);

             
             //city name
             NSString *cityName=addressDictionary[@"City"];
             [city addObject:[NSString stringWithFormat:@"%@",cityName]];
             NSLog(@"%@",cityName);
             
             //state name
             NSString *state=placemark.administrativeArea;
             [city addObject:[NSString stringWithFormat:@"%@",state]];
             NSLog(@"%@",state);
             
             //formatted address
             NSArray *show= addressDictionary[@"FormattedAddressLines"];
             NSLog(@"%@",show);
             country = [show componentsJoinedByString:@",\n" ] ;
             NSLog(@"%@",country);
             [city addObject:country];
             
             //name
             NSString *area=placemark.country;
             [city addObject:[NSString stringWithFormat:@"%@",area]];
             NSLog(@"%@",area);
            
             
             //setting marker
             GMSMarker *markerall = [GMSMarker markerWithPosition:newLocation];
             markerall.title = country;
             markerall.map = googleMap;
             markerall.icon = [GMSMarker markerImageWithColor:[UIColor greenColor]];
             
             //latitude longitude
            NSString *latitude = [NSString stringWithFormat:@"%f", newLocation.latitude  ];
             [city addObject:[NSString stringWithFormat:@"%@",latitude]];
             NSString *longitude = [NSString stringWithFormat:@"%f", newLocation.longitude  ];
             [city addObject:[NSString stringWithFormat:@"%@",longitude]];

         }
     }];
    return country;
}


- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self getlocation:coordinate];
    [googleMap clear];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
