//
//  ViewController.h
//  googleLocation
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "DestinationViewController.h"
@interface ViewController : UIViewController
<GMSMapViewDelegate,CLLocationManagerDelegate>


@end

