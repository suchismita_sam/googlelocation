//
//  AppDelegate.h
//  googleLocation
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

