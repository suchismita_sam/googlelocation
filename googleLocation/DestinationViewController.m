//
//  DestinationViewController.m
//  googleLocation
//
//  Created by Click Labs134 on 11/4/15.
//  Copyright © 2015 clicklabs. All rights reserved.
//

#import "DestinationViewController.h"

@interface DestinationViewController ()
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *permanentAddress;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabelDisplay;
@property (strong, nonatomic) IBOutlet UILabel *nameLabelDisplay;
@property (strong, nonatomic) IBOutlet UILabel *stateLabelDisplay;
@property (strong, nonatomic) IBOutlet UILabel *cityLabelDisplay;
@property (strong, nonatomic) IBOutlet UILabel *latitudeLabelDisplay;
@property (strong, nonatomic) IBOutlet UILabel *permanentLabelDisplay;
@property (strong, nonatomic) IBOutlet UILabel *inIndiaOrNot;

@end

@implementation DestinationViewController
@synthesize nameLabel;
@synthesize stateLabel;
@synthesize cityLabel;
@synthesize latitudeLabel;
@synthesize longitudeLabel;
@synthesize permanentAddress;
@synthesize destdata;
@synthesize longitudeLabelDisplay;
@synthesize latitudeLabelDisplay;
@synthesize permanentLabelDisplay;
@synthesize nameLabelDisplay;
@synthesize stateLabelDisplay;
@synthesize cityLabelDisplay;
@synthesize inIndiaOrNot;

- (void)viewDidLoad {
    [super viewDidLoad];

    
    NSString *str1=[destdata objectAtIndex:0];
    NSLog(@"%@",str1);
    if ([str1 isEqualToString:@"(null)"])
    {
        cityLabelDisplay.text=[NSString stringWithFormat:@"NA",str1];
    }
    else
    {
        cityLabelDisplay.text=[NSString stringWithFormat:@"%@",str1];
    }

    
    NSString *str2=[destdata objectAtIndex:1];
    NSLog(@"%@",str2);
    if ([str1 isEqualToString:@"(null)"])
    {
        stateLabelDisplay.text=[NSString stringWithFormat:@"NA",str2];
    }
    else
    {
        stateLabelDisplay.text=[NSString stringWithFormat:@"%@",str2];
    }
    
    NSString *str3=[destdata objectAtIndex:2];
    NSLog(@"%@",str3);
    if ([str3 isEqualToString:@"(null)"])
    {
        permanentLabelDisplay.text=[NSString stringWithFormat:@"NA",str3];
    }
    else
    {
        permanentLabelDisplay.text=[NSString stringWithFormat:@"%@",str3];
    }

    NSString *str4=[destdata objectAtIndex:3];
    NSLog(@"%@",str4);
    if ([str4 isEqualToString:@"(null)"])
    {
        nameLabelDisplay.text=[NSString stringWithFormat:@"NA",str4];
    }
    else
    {
        nameLabelDisplay.text=[NSString stringWithFormat:@"%@",str4];
    }
    if ([str4 isEqualToString:@"India"])
    {
        inIndiaOrNot.text=@"In India";
    }
    else
    {
        inIndiaOrNot.text=@"Outside India";
    }

    NSString *str5=[destdata objectAtIndex:4];
    NSLog(@"%@",str5);
    if ([str5 isEqualToString:@"(null)"])
    {
        latitudeLabelDisplay.text=[NSString stringWithFormat:@"NA",str5];
    }
    else
    {
        latitudeLabelDisplay.text=[NSString stringWithFormat:@"%@",str5];
    }

    NSString *str6=[destdata objectAtIndex:5];
    NSLog(@"%@",str6);
    longitudeLabelDisplay.text=[NSString stringWithFormat:@"%@",str6];
    if ([str6 isEqualToString:@"(null)"])
    {
        longitudeLabelDisplay.text=[NSString stringWithFormat:@"NA",str6];
    }
    else
    {
        longitudeLabelDisplay.text=[NSString stringWithFormat:@"%@",str6];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
